Prinata Rakha Santoso
2006485693

Apakah perbedaan antara JSON dan XML?
- XML adalah suatu markup language, sedangkan JSON bertujuan untuk membuat karakteristik dari suatu objek.
- lebih lama untuk membuat file XML karena perlu menulis tag pembuka dan tag penutup, sedangkan JSON hanya perlu menuliskan key nya saja.
- tag di XML merupakan key di JSON.
- apabila data yang ingin ditulis besar, maka penggunaan XML tidak praktis dan susah dilihat, sedangkan JSON praktis dan simple.
- JSON dapat merepresentasikan data dalam bentuk array ([]), sedangkan XML tidak bisa.


Apakah perbedaan antara HTML dan XML?
- HTML tidak bisa transfer data, XML bisa.
- HTML tidak case sensitive, XML sebaliknya.
- Tidak semua tag di HTML memiliki tag penutup, XML selalu memiliki tag penutup.
- tag sudah ditentukan di HTML, sedangkan tag di XML ditentukan (dibuat) sendiri oleh user.
- tag di HTML berfungsi untuk display data, tag di XML berfungsi untuk mendeskripsikan data.

