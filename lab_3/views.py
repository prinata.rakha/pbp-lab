from django.shortcuts import redirect, render
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required

@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all().values()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'lab3_index.html', response)

@login_required(login_url='/admin/login/')
def add_friend(request):
    form = FriendForm()

    if request.method == "POST":
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(index)
    context = {'form': form}
    return render(request, "lab3_form.html", context)

# referensi form: https://www.youtube.com/watch?v=VOddmV4Xl1g
# referensi redirect: https://www.tutorialspoint.com/django/django_page_redirection.htm
# referensi login_required: https://stackoverflow.com/questions/3578882/how-to-specify-the-login-required-redirect-url-in-django