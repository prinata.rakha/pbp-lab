from django import forms
# from django.db.models import fields
from lab_1.models import Friend

class DateInput(forms.DateInput):
    input_type = 'date'

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        # fields = ['name', 'npm', 'DOB']
        # bisa pake yg atas atau bawah, 
        # tp kl mau pake yg atas, harus import dr db.models
        fields = '__all__'
        widgets = {'DOB' : DateInput}