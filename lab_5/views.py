from lab_2.models import Note
from django.shortcuts import redirect, render
# from .forms import NoteForm

# Create your views here.

def index(request):
    notes = Note.objects.all().values()  
    response = {'notes': notes}
    return render(request, 'lab5_index.html', response)

# def add_note(request):
#     form = NoteForm()

#     if request.method == "POST":
#         form = NoteForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect(index)
#     context = {'form': form}
#     return render(request, "lab4_form.html", context)

# def note_list(request):
#     notes = Note.objects.all().values()  
#     response = {'notes': notes}
#     return render(request, 'lab4_note_list.html', response)

# def navbar(request):
#     return render(request, 'lab4_navbar.html')

