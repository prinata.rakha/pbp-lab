from django import forms
from lab_2.models import Note
from django.db.models import fields

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        # fields = '__all__'
        fields = ['to', 'From', 'title', 'message']