import 'package:flutter/material.dart';
import 'lokasi.dart';
import 'lokasi_card.dart';
import 'lokasi_form.dart';

void main() => runApp(MaterialApp(
      home: Vaksin(),
    ));

class Vaksin extends StatelessWidget {
  Vaksin({Key? key}) : super(key: key);

  List<Lokasi> locationsJak = [
    Lokasi('RS Jakarta', 'Jl. Rumah Sakit', '0811', 'sinovac', 'umur 18 tahun'),
    Lokasi(
        'RS Fatmawati', 'Jl. Fatmawati', '0813', 'astrazeneca', 'umur 8 tahun'),
  ];
  List<Lokasi> locationsBog = [];
  List<Lokasi> locationsDep = [
    Lokasi('RS Depok', 'Jl. Depok Raya', '0214', 'pfizer', '-'),
  ];
  List<Lokasi> locationsTag = [];
  List<Lokasi> locationsBek = [
    Lokasi('RS Bekasi', 'Jl. Bekasi Raya', '0214', 'pfizer', '-'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('PBP D-05'),
        backgroundColor: Colors.blue[200],
        actions: <Widget>[
          IconButton(
            // buat masuk ke halaman form
            icon: const Icon(Icons.navigate_next),
            tooltip: 'Go to the next page',
            onPressed: () {
              Navigator.push(context, MaterialPageRoute<void>(
                builder: (BuildContext context) {
                  return Scaffold(
                    body: LokasiForm(),
                  );
                },
              ));
            },
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 0, 0, 15),
          child: Column(
            children: <Widget>[
              Container(
                padding: const EdgeInsets.all(20),
                child: const Text(
                  'Vaksin',
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.w600,
                    letterSpacing: 2,
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
                child: const Text(
                  'Dibawah ini adalah lokasi vaksin di Jabodetabek. Dibawah ini adalah lokasi vaksin di Jabodetabek. Dibawah ini adalah lokasi vaksin di Jabodetabek. Dibawah ini adalah lokasi vaksin di Jabodetabek. Dibawah ini adalah lokasi vaksin di Jabodetabek. ',
                  style: TextStyle(
                    letterSpacing: 1,
                    fontSize: 12,
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(20, 40, 20, 0),
                child: const Center(
                    child: Text(
                  'Jakarta',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    letterSpacing: 2,
                  ),
                )),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children:
                    locationsJak.map((lokasi) => LokasiCard(lokasi)).toList(),
              ),
              Container(
                padding: const EdgeInsets.all(20),
                child: const Center(
                    child: Text(
                  'Bogor',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    letterSpacing: 2,
                  ),
                )),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children:
                    locationsBog.map((lokasi) => LokasiCard(lokasi)).toList(),
              ),
              Container(
                padding: const EdgeInsets.all(20),
                child: const Center(
                    child: Text(
                  'Depok',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    letterSpacing: 2,
                  ),
                )),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children:
                    locationsDep.map((lokasi) => LokasiCard(lokasi)).toList(),
              ),
              Container(
                padding: const EdgeInsets.all(20),
                child: const Center(
                    child: Text(
                  'Tangerang',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    letterSpacing: 2,
                  ),
                )),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children:
                    locationsTag.map((lokasi) => LokasiCard(lokasi)).toList(),
              ),
              Container(
                padding: const EdgeInsets.all(20),
                child: const Center(
                    child: Text(
                  'Bekasi',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    letterSpacing: 2,
                  ),
                )),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children:
                    locationsBek.map((lokasi) => LokasiCard(lokasi)).toList(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
