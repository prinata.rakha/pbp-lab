class Lokasi {
  String nama;
  String alamat;
  String nomor;
  String jenis;
  String syarat;

  Lokasi(this.nama, this.alamat, this.nomor, this.jenis, this.syarat);
}

// Lokasi({String nama, String nomor, String jenis, String syarat}) {
//     this.nama = nama;
//     this.nomor = nomor;
//     this.jenis = jenis;
//     this.syarat = syarat;
//   }

// Lokasi lokasi = Lokasi("nama", 'nomor', 'jenis', 'syarat');

// locations
//                   .map((lokasi) => Text('${lokasi.nama} - ${lokasi.alamat}'))
//                   .toList(),