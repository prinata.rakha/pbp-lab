import 'package:flutter/material.dart';
import 'lokasi.dart';

class LokasiCard extends StatelessWidget {
  final Lokasi lokasi; // data statis
  // ignore: use_key_in_widget_constructors
  const LokasiCard(this.lokasi);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.fromLTRB(20, 16, 20, 0),
      color: Colors.grey[200],
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              lokasi.nama,
              style: const TextStyle(
                fontSize: 17,
                letterSpacing: 1,
              ),
            ),
            const SizedBox(
              height: 6,
            ),
            Text(
              'Alamat: ${lokasi.alamat}',
              style: TextStyle(
                fontSize: 14,
                color: Colors.grey[800],
                letterSpacing: 0.5,
              ),
            ),
            Text(
              'No. Telepon: ${lokasi.nomor}',
              style: TextStyle(
                fontSize: 14,
                color: Colors.grey[800],
                letterSpacing: 0.5,
              ),
            ),
            Text(
              'Jenis: ${lokasi.jenis}',
              style: TextStyle(
                fontSize: 14,
                color: Colors.grey[800],
                letterSpacing: 0.5,
              ),
            ),
            Text(
              'Syarat: ${lokasi.syarat}',
              style: TextStyle(
                fontSize: 14,
                color: Colors.grey[800],
                letterSpacing: 0.5,
              ),
            ),
            ElevatedButton(
              onPressed: null,
              child: Text('Maps'),
              style: ElevatedButton.styleFrom(
                  primary: Colors.black,
                  onPrimary: Colors.white,
                  shape: const RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5)),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
